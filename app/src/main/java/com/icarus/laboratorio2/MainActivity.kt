package com.icarus.laboratorio2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener {
            var msgGeneracion:String=""
            var msgMarco:String=""
            var msgPoblacion:String=""
            var msgCircunstancia:String=""

            if(!edtAnno.text.toString().isEmpty()){
                val anno=edtAnno.text.toString().toInt()
                if(anno in 1969..1980){
                    msgGeneracion="Generacion X"
                    msgMarco="1969 a 1980"
                    msgPoblacion="9'300,000"
                    msgCircunstancia="Crisis del 73 y transición española"
                    imvRasgo.setImageResource(R.drawable.ico_obsesion)
                }
                else if(anno in 1981..1993){
                    msgGeneracion="Generacion Y"
                    msgMarco="1981 a 1993"
                    msgPoblacion="7'200,000"
                    msgCircunstancia="Inicio de la digitación"
                    imvRasgo.setImageResource(R.drawable.ico_frustracion)
                }
                else if(anno in 1994..2010){
                    msgGeneracion="Genracion Z"
                    msgMarco="1994 a 2010"
                    msgPoblacion="7'800,000"
                    msgCircunstancia="Expansión masiva de Internet"
                    imvRasgo.setImageResource(R.drawable.ico_irreverente)
                }
                else if(anno in 1949..1968){
                    msgGeneracion="Baby Boom"
                    msgMarco="1949 a 1968"
                    msgPoblacion="12'200,000"
                    msgCircunstancia="Paz y explosión demográfica"
                    imvRasgo.setImageResource(R.drawable.ico_ambicioso)
                }
                else if(anno in 1930..1948){
                    msgGeneracion="Silent Generation"
                    msgMarco="1930 a 1948"
                    msgPoblacion="6'300,000"
                    msgCircunstancia="Conflictos bélicos"
                    imvRasgo.setImageResource(R.drawable.ico_austeridad)
                }
                else {
                    Toast.makeText(this, "El año ingresado no está registrado", Toast.LENGTH_SHORT).show();
                    msgGeneracion = ""
                    msgMarco=""
                    msgCircunstancia=""
                    msgPoblacion=""
                    imvRasgo.setImageResource(R.drawable.ico_nada)
                }
            }else{
                Toast.makeText(this, "Escriba un año para evaluar", Toast.LENGTH_SHORT).show();
                msgGeneracion=""
                imvRasgo.setImageResource(R.drawable.ico_nada)
            }
            tvGeneracion.text=msgGeneracion            
            tvCirHistorica.text=msgCircunstancia
            tvMarco.text=msgMarco
            tvPoblacion.text=msgPoblacion
        }

    }
}